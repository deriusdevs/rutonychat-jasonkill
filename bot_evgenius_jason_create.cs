using System.IO;
using System;
using System.Collections.Generic;
using System.Threading;

namespace RutonyChat {
    public class Script {
        public void RunScript(string site, string username, string text, string param) {

        	string[] jasons = {"Джейсон часть 1", "Джейсон часть 2", "Джейсон часть 3", "Джейсон X"};
			string filename = ProgramProps.dir_scripts + @"\jason.txt";

			if (File.Exists(filename))
			{
                RutonyBot.BotSay(site, "Джейсон уже создан!");
				return;
			}

			Random rnd = new Random();
			int randomJason = rnd.Next(1, 5);

			string selected_jason = jasons[randomJason-1];

			RutonyBotFunctions.FileAddString(filename, string.Format("{0}", selected_jason));
			
			RutonyBot.BotSay(site, string.Format("{0} вернулся! Избивайте его, иначе умрете!", selected_jason));

			new Thread(() => {
				Thread.CurrentThread.IsBackground = true;
				Thread.Sleep(160000);

        		if (File.Exists(filename))
				{
					try {
						File.Delete(filename);
			    	} catch { }
				
					RutonyBot.BotSay(site, "Вы не смогли убить джейсона и он улетел на марс!");
				}

			}).Start();

			return;
		}	
    }
}