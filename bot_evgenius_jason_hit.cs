using System.IO;
using System;
using System.Collections.Generic;
using System.Threading;

namespace RutonyChat {
    public class Script {
        public void RunScript(string site, string username, string text, string param) {

        	string file = ProgramProps.dir_scripts + @"\jason.txt";

        	if (!File.Exists(file))
			{
                RutonyBot.BotSay(site, "Джейсон еще не пришел! Kappa");
				return;
			}

        	string[] weapons = {"битой", "топором", "кастрюлей", "пиписькой", "палкой", "веткой", "трубой", "ключем", "сковородой"};
        	string[] body = {"", "по руке", "по голове", "по щеке", "по пузику", "в мизинец"};

			Random rnd = new Random();
			int randomHit = rnd.Next(1, 2);
			int randomJason = rnd.Next(1, 30);
			int randomWeapon = rnd.Next(0, 8);
			int randomBody = rnd.Next(1, 5);

			string selectedWeapon = weapons[randomWeapon];
			string selectedBody = body[randomBody];

			if(randomHit == randomJason)
			{
				RutonyBot.BotSay(site, string.Format("@{0}, бьет Джейсона {1} {2}, он падает и не встает. Это победа!", username,selectedWeapon,selectedBody));
				File.Delete(file);
			}
			else
			{
				RutonyBot.BotSay(site, string.Format("@{0} бьет Джейсона {1} {2}, но он все еще на ногах!", username, selectedWeapon, selectedBody));
			}
		}	
    }
}